package com.example.examenrt;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class KafkaToCSV {

    public static void main(String[] args) {
        String bootstrapServers = "localhost:9092";
        String groupId = "org.example.examenrt.KafkaToCV";
        String topic = "db";
        String csvFilePath = "donnees_kafka.csv";

        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        try (Consumer<String, String> consumer = new KafkaConsumer<>(properties);
             FileWriter csvWriter = new FileWriter(csvFilePath)) {

            consumer.subscribe(Collections.singletonList(topic));

            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

                records.forEach(record -> {
                    String value = record.value();

                    // Traitement des données (vous devez adapter cette partie selon le format de vos données)
                    String[] data = value.split(",");

                    // Écrire les données dans le fichier CSV
                    try {
                        for (String datum : data) {
                            csvWriter.append(datum).append(",");
                        }
                        csvWriter.append("\n");
                        csvWriter.flush();
                        System.out.println("Données enregistrées dans " + csvFilePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
